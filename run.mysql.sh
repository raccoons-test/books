#!/bin/bash

docker system prune --all --volumes --force
docker network create --attachable books
docker run -d -p 3306:3306 --net books -e MYSQL_ROOT_PASSWORD=RFr2bRnH9mRXaE8z --name books-mysql mysql:5.7.27
docker run -d -p 8080:80 --net books --link books-mysql -e PMA_ARBITRARY=1 --name books-pma phpmyadmin/phpmyadmin
