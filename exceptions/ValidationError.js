class ValidationError extends Error {

    constructor (message, body) {
        super(message)
        this.body = body
        this.status = 405
        this.message = message
        this.name = 'ValidationError'
    }

}

module.exports = ValidationError
