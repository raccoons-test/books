const mysql = require('mysql')

let connect = true
const connection = mysql.createConnection(require('./config/db'))

module.exports = function () {
    if (connect) {
        connection.connect(err => {
            if (err) throw err
        })
        connect = false
    }

    return connection
}
