module.exports = (ctx, next) => next().catch(err => {
    ctx.status = err.status || 500
    ctx.body = err.message || err.body || {}
})
