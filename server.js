require('./db')()
const Koa = require('koa')
const Router = require('koa-router')
const errorHandler = require('./middlewares/errorHandler')

const server = new Koa()
const router = new Router()

const booksRouter = require('./routes/books')
router.use(booksRouter.routes(), booksRouter.allowedMethods())

server.use(errorHandler)
server.use(router.routes())
server.use(router.allowedMethods())
server.listen(+process.env.PORT || 3000)
