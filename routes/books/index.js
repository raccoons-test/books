const Router = require('koa-router')
const bodyParser = require('koa-body')()

const router = new Router()

router.get('/books', require('./read'))
router.post('/book', bodyParser, require('./create'))
router.put('/book/:id', bodyParser, require('./update'))

module.exports = router
