const qs = require('qs')
const Book = require('../../models/book')

module.exports = async ctx => {
    ctx.body = await Book.findMany(qs.parse(ctx.querystring))
}
