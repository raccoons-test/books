const Book = require('../../models/book')

module.exports = async ctx => {
    if (!+ctx.params.id) throw new Error('ID is required.')

    let book = new Book({...ctx.request.body, id: ctx.params.id})
    book.validate()
    ctx.body = await book.save()
}
