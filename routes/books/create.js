const Book = require('../../models/book')

module.exports = async ctx => {
    let book = new Book({...ctx.request.body, id: null})
    book.validate()
    ctx.body = await book.save()
}
