const db = require('../db')()
const ValidationError = require('../exceptions/ValidationError')

const escapeField = v => db.escape(v).replace(/\'/g, '`')

class Book {

    constructor ({id = null, title = null, date = null, author = null, description = null, image = null} = {}) {
        this.id = id
        this.date = date
        this.title = title
        this.image = image
        this.author = author
        this.description = description
    }

    static get table () {
        return 'books'
    }

    static get fields () {
        return ['title', 'author', 'description', 'date', 'image']
    }

    validate () {
        let errors = {}

        if (!this.title) {
            errors.title = 'Title is required.'
        } else if (this.title.length < 8) {
            errors.title = 'Title should be at least 8 symbols long.'
        } else if (this.title.length > 255) {
            erorrs.title = 'Title should not be greater than 255 symbols.'
        }

        if (!this.author) {
            errors.author = 'Author is required.'
        } else if (this.author.length > 255) {
            erorrs.author = 'Author should not be greater than 255 symbols.'
        }

        if (Object.keys(errors).length)
            throw new ValidationError(null, errors)
    }

    static findMany (options) {
        return new Promise((resolve, reject) => {
            let fields = ['id', ...this.fields]
            let sql = `SELECT * FROM \`${Book.table}\``

            let wheres = []

            if (options.filter) {
                wheres = Object.keys(options.filter)
                               .filter(v => fields.includes(v))
                               .map(k => ''.concat(escapeField(k), ' LIKE ', db.escape(options.filter[k])))
            }

            if (wheres.length) {
                sql = sql.concat(' WHERE ', wheres.reduce((s, w, i) => s.concat(i > 0 ? ' AND ' : '', w), ''))
            }

            if (
                options.orderBy && options.orderBy.field &&
                fields.includes(options.orderBy.field)
            ) {
                options.orderBy.type = ['asc', 'desc'].find(s => s === options.orderBy.type.toLowerCase())
                sql = sql.concat(
                    ' ORDER BY ', escapeField(options.orderBy.field),
                    options.orderBy.type ? ' ' + options.orderBy.type : ''
                )
            }

            if (options.limit) {
                sql = sql.concat(' LIMIT ', +options.limit)
            }

            if (options.offset) {
                sql = sql.concat(' OFFSET ', +options.offset);
            }

            db.query(sql, (err, res) => {
                if (err) reject(err)
                else resolve(res.map(i => new Book(i)))
            })
        })
    }

    save () {
        return new Promise((resolve, reject) => {
            let values = Book.fields.reduce((o, p) => (o[p] = this[p], o), {})

            /**
             * If `id` specified then UPDATE,
             * perform INSERT otherwise.
             */

            if (!this.id) {
                db.query(`INSERT INTO \`${Book.table}\` SET ?`, values, (err, res) => {
                    if (err) reject(err)
                    else {
                        this.id = res.insertId
                        resolve(this)
                    }
                })
            } else {
                db.query(`UPDATE \`${Book.table}\` SET ? WHERE id = ?`, [values, this.id], err => {
                    if (err) reject(err)
                    else resolve(this)
                })
            }
        })
    }

}

module.exports = Book
