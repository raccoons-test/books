Test Project
===

Requirements
---

1. NodeJS
2. Docker

Configuration
---

Configuration provided using the following environment variables:

* `DB_HOST` specifies database server host address
* `DB_PORT` specifies database server port
* `DB_USER` specifies database connection user
* `DB_PASSWORD` specifies database connection password
* `DB_DATABASE` specifies default database

Run
---

1. Install dependencies via `npm install`

2. To set-up database run `run.mysql.sh`

    2.1. Upload default mysql dump `db.sql`
    
3. Run server using one of the following commands
    
    3.1. `npm start`
    
    3.2. `npm run watch` (nodemon required)

Troubleshooting
---

#### Server can't connect to database

Be sure to specify correct database credentials, also take in
account that server reads configuration from environment variables.

Example of running server: `export $(cat .env | xargs) && npm start`
